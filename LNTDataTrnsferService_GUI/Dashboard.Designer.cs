﻿namespace LNTDataTrnsferService_GUI
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnDataTrnsferApply = new System.Windows.Forms.Button();
            this.txtDataTransferTime = new System.Windows.Forms.MaskedTextBox();
            this.lstDataTransferTime = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btbAddDataTransferTime = new System.Windows.Forms.Button();
            this.ViewLog = new System.Windows.Forms.LinkLabel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.btnDbConfig1 = new System.Windows.Forms.Button();
            this.btnDbConfig2 = new System.Windows.Forms.Button();
            this.txtDataTransferTime1 = new System.Windows.Forms.TextBox();
            this.dbFTPConfig = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnApplyTime = new System.Windows.Forms.Button();
            this.lstFtpTime = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddFtpTime = new System.Windows.Forms.Button();
            this.txtFtpTime = new System.Windows.Forms.TextBox();
            this.txtDTInterval = new System.Windows.Forms.MaskedTextBox();
            this.BtnApplyInterval = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.txtDataTransferTime1);
            this.panel1.Controls.Add(this.dbFTPConfig);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(612, 489);
            this.panel1.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.txtDTInterval);
            this.panel5.Controls.Add(this.BtnApplyInterval);
            this.panel5.Controls.Add(this.panel3);
            this.panel5.Controls.Add(this.ViewLog);
            this.panel5.Location = new System.Drawing.Point(336, 29);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(190, 363);
            this.panel5.TabIndex = 11;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnDataTrnsferApply);
            this.panel3.Controls.Add(this.txtDataTransferTime);
            this.panel3.Controls.Add(this.lstDataTransferTime);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.btbAddDataTransferTime);
            this.panel3.Location = new System.Drawing.Point(4, 101);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(164, 190);
            this.panel3.TabIndex = 5;
            this.panel3.Visible = false;
            // 
            // btnDataTrnsferApply
            // 
            this.btnDataTrnsferApply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnDataTrnsferApply.Location = new System.Drawing.Point(76, 118);
            this.btnDataTrnsferApply.Margin = new System.Windows.Forms.Padding(4);
            this.btnDataTrnsferApply.Name = "btnDataTrnsferApply";
            this.btnDataTrnsferApply.Size = new System.Drawing.Size(63, 28);
            this.btnDataTrnsferApply.TabIndex = 5;
            this.btnDataTrnsferApply.Text = "Apply";
            this.btnDataTrnsferApply.UseVisualStyleBackColor = false;
            this.btnDataTrnsferApply.Click += new System.EventHandler(this.btnDataTrnsferApply_Click);
            // 
            // txtDataTransferTime
            // 
            this.txtDataTransferTime.Location = new System.Drawing.Point(28, 53);
            this.txtDataTransferTime.Mask = "00:00";
            this.txtDataTransferTime.Name = "txtDataTransferTime";
            this.txtDataTransferTime.Size = new System.Drawing.Size(47, 22);
            this.txtDataTransferTime.TabIndex = 5;
            this.txtDataTransferTime.ValidatingType = typeof(System.DateTime);
            // 
            // lstDataTransferTime
            // 
            this.lstDataTransferTime.FormattingEnabled = true;
            this.lstDataTransferTime.ItemHeight = 16;
            this.lstDataTransferTime.Location = new System.Drawing.Point(28, 84);
            this.lstDataTransferTime.Margin = new System.Windows.Forms.Padding(4);
            this.lstDataTransferTime.Name = "lstDataTransferTime";
            this.lstDataTransferTime.Size = new System.Drawing.Size(47, 116);
            this.lstDataTransferTime.TabIndex = 4;
            this.lstDataTransferTime.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstDataTransferTime_MouseDoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data Transfer Time";
            // 
            // btbAddDataTransferTime
            // 
            this.btbAddDataTransferTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btbAddDataTransferTime.Location = new System.Drawing.Point(76, 50);
            this.btbAddDataTransferTime.Margin = new System.Windows.Forms.Padding(4);
            this.btbAddDataTransferTime.Name = "btbAddDataTransferTime";
            this.btbAddDataTransferTime.Size = new System.Drawing.Size(63, 28);
            this.btbAddDataTransferTime.TabIndex = 3;
            this.btbAddDataTransferTime.Text = "Add";
            this.btbAddDataTransferTime.UseVisualStyleBackColor = false;
            this.btbAddDataTransferTime.Click += new System.EventHandler(this.btbAddDataTransferTime_Click);
            // 
            // ViewLog
            // 
            this.ViewLog.AutoSize = true;
            this.ViewLog.Location = new System.Drawing.Point(57, 295);
            this.ViewLog.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ViewLog.Name = "ViewLog";
            this.ViewLog.Size = new System.Drawing.Size(75, 17);
            this.ViewLog.TabIndex = 9;
            this.ViewLog.TabStop = true;
            this.ViewLog.Text = "Check Log";
            this.ViewLog.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ViewLog_LinkClicked);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Controls.Add(this.btnStartStop);
            this.panel4.Controls.Add(this.btnDbConfig1);
            this.panel4.Controls.Add(this.btnDbConfig2);
            this.panel4.Location = new System.Drawing.Point(88, 29);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(218, 363);
            this.panel4.TabIndex = 10;
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Location = new System.Drawing.Point(7, 277);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(197, 76);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnStartStop
            // 
            this.btnStartStop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStartStop.BackgroundImage")));
            this.btnStartStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStartStop.Location = new System.Drawing.Point(7, 7);
            this.btnStartStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(197, 76);
            this.btnStartStop.TabIndex = 0;
            this.btnStartStop.Text = "Start";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // btnDbConfig1
            // 
            this.btnDbConfig1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDbConfig1.BackgroundImage")));
            this.btnDbConfig1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDbConfig1.Location = new System.Drawing.Point(7, 96);
            this.btnDbConfig1.Margin = new System.Windows.Forms.Padding(4);
            this.btnDbConfig1.Name = "btnDbConfig1";
            this.btnDbConfig1.Size = new System.Drawing.Size(197, 76);
            this.btnDbConfig1.TabIndex = 6;
            this.btnDbConfig1.Text = "Source Database Configuration";
            this.btnDbConfig1.UseVisualStyleBackColor = true;
            this.btnDbConfig1.Click += new System.EventHandler(this.btnDbConfig1_Click);
            // 
            // btnDbConfig2
            // 
            this.btnDbConfig2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDbConfig2.BackgroundImage")));
            this.btnDbConfig2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDbConfig2.Location = new System.Drawing.Point(7, 185);
            this.btnDbConfig2.Margin = new System.Windows.Forms.Padding(4);
            this.btnDbConfig2.Name = "btnDbConfig2";
            this.btnDbConfig2.Size = new System.Drawing.Size(197, 76);
            this.btnDbConfig2.TabIndex = 7;
            this.btnDbConfig2.Text = "Destination Database Configuration";
            this.btnDbConfig2.UseVisualStyleBackColor = true;
            this.btnDbConfig2.Click += new System.EventHandler(this.btnDbConfig2_Click);
            // 
            // txtDataTransferTime1
            // 
            this.txtDataTransferTime1.Location = new System.Drawing.Point(428, 360);
            this.txtDataTransferTime1.Margin = new System.Windows.Forms.Padding(4);
            this.txtDataTransferTime1.Name = "txtDataTransferTime1";
            this.txtDataTransferTime1.Size = new System.Drawing.Size(47, 22);
            this.txtDataTransferTime1.TabIndex = 2;
            this.txtDataTransferTime1.Text = "00:00";
            this.txtDataTransferTime1.Visible = false;
            // 
            // dbFTPConfig
            // 
            this.dbFTPConfig.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dbFTPConfig.BackgroundImage")));
            this.dbFTPConfig.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.dbFTPConfig.Location = new System.Drawing.Point(402, 293);
            this.dbFTPConfig.Margin = new System.Windows.Forms.Padding(4);
            this.dbFTPConfig.Name = "dbFTPConfig";
            this.dbFTPConfig.Size = new System.Drawing.Size(124, 60);
            this.dbFTPConfig.TabIndex = 7;
            this.dbFTPConfig.Text = "FTP Configuration";
            this.dbFTPConfig.UseVisualStyleBackColor = true;
            this.dbFTPConfig.Visible = false;
            this.dbFTPConfig.Click += new System.EventHandler(this.dbFTPConfig_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnApplyTime);
            this.panel2.Controls.Add(this.lstFtpTime);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnAddFtpTime);
            this.panel2.Controls.Add(this.txtFtpTime);
            this.panel2.Location = new System.Drawing.Point(663, 110);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(186, 226);
            this.panel2.TabIndex = 6;
            this.panel2.Visible = false;
            // 
            // btnApplyTime
            // 
            this.btnApplyTime.Location = new System.Drawing.Point(77, 128);
            this.btnApplyTime.Margin = new System.Windows.Forms.Padding(4);
            this.btnApplyTime.Name = "btnApplyTime";
            this.btnApplyTime.Size = new System.Drawing.Size(63, 28);
            this.btnApplyTime.TabIndex = 5;
            this.btnApplyTime.Text = "Apply";
            this.btnApplyTime.UseVisualStyleBackColor = true;
            this.btnApplyTime.Click += new System.EventHandler(this.btnApplyTime_Click);
            // 
            // lstFtpTime
            // 
            this.lstFtpTime.FormattingEnabled = true;
            this.lstFtpTime.ItemHeight = 16;
            this.lstFtpTime.Location = new System.Drawing.Point(17, 82);
            this.lstFtpTime.Margin = new System.Windows.Forms.Padding(4);
            this.lstFtpTime.Name = "lstFtpTime";
            this.lstFtpTime.Size = new System.Drawing.Size(47, 116);
            this.lstFtpTime.TabIndex = 4;
            this.lstFtpTime.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstFtpTime_MouseDoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "FTP Transfer Time";
            // 
            // btnAddFtpTime
            // 
            this.btnAddFtpTime.Location = new System.Drawing.Point(77, 49);
            this.btnAddFtpTime.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddFtpTime.Name = "btnAddFtpTime";
            this.btnAddFtpTime.Size = new System.Drawing.Size(63, 28);
            this.btnAddFtpTime.TabIndex = 3;
            this.btnAddFtpTime.Text = "Add";
            this.btnAddFtpTime.UseVisualStyleBackColor = true;
            this.btnAddFtpTime.Click += new System.EventHandler(this.btnAddFtpTime_Click);
            // 
            // txtFtpTime
            // 
            this.txtFtpTime.Location = new System.Drawing.Point(17, 50);
            this.txtFtpTime.Margin = new System.Windows.Forms.Padding(4);
            this.txtFtpTime.Name = "txtFtpTime";
            this.txtFtpTime.Size = new System.Drawing.Size(47, 22);
            this.txtFtpTime.TabIndex = 2;
            this.txtFtpTime.Text = "00:00";
            // 
            // txtDTInterval
            // 
            this.txtDTInterval.Location = new System.Drawing.Point(32, 41);
            this.txtDTInterval.Mask = "00";
            this.txtDTInterval.Name = "txtDTInterval";
            this.txtDTInterval.Size = new System.Drawing.Size(26, 22);
            this.txtDTInterval.TabIndex = 11;
            // 
            // BtnApplyInterval
            // 
            this.BtnApplyInterval.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.BtnApplyInterval.Location = new System.Drawing.Point(89, 36);
            this.BtnApplyInterval.Margin = new System.Windows.Forms.Padding(4);
            this.BtnApplyInterval.Name = "BtnApplyInterval";
            this.BtnApplyInterval.Size = new System.Drawing.Size(63, 28);
            this.BtnApplyInterval.TabIndex = 10;
            this.BtnApplyInterval.Text = "Apply";
            this.BtnApplyInterval.UseVisualStyleBackColor = false;
            this.BtnApplyInterval.Click += new System.EventHandler(this.BtnApplyInterval_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Data Transfer Interval";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(26, 62);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "(MM)";
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(612, 489);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LNTDataTrnsferService Service";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnStartStop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button dbFTPConfig;
        private System.Windows.Forms.Button btnDbConfig2;
        private System.Windows.Forms.Button btnDbConfig1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListBox lstDataTransferTime;
        private System.Windows.Forms.Button btbAddDataTransferTime;
        private System.Windows.Forms.TextBox txtDataTransferTime1;
        private System.Windows.Forms.Button btnDataTrnsferApply;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnApplyTime;
        private System.Windows.Forms.ListBox lstFtpTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAddFtpTime;
        private System.Windows.Forms.TextBox txtFtpTime;
        private System.Windows.Forms.LinkLabel ViewLog;
        private System.Windows.Forms.MaskedTextBox txtDataTransferTime;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox txtDTInterval;
        private System.Windows.Forms.Button BtnApplyInterval;
        private System.Windows.Forms.Label label4;
    }
}