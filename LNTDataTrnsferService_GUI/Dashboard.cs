﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LNTDataTrnsferService_GUI
{
    public partial class Dashboard : Form
    {
        string filePath = Directory.GetCurrentDirectory();
        string[] arrConString = File.ReadAllLines("DBConnection1.TimWatch");
        string sPath = Application.StartupPath;
        public Dashboard()
        {
            InitializeComponent();
        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceController service = new ServiceController("LNTDataTrnsferService");

                if ((service.Status.Equals(ServiceControllerStatus.Stopped)) || (service.Status.Equals(ServiceControllerStatus.StopPending)))
                {
                    service.Start();
                    btnStartStop.Text = "Stop";
                }
                else
                {
                    service.Stop();
                    btnStartStop.Text = "Start";
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnDbConfig1_Click(object sender, EventArgs e)
        {
            dbConfig1 dbcon1 = new dbConfig1();
            dbcon1.ShowDialog();
        }

        private void btnDbConfig2_Click(object sender, EventArgs e)
        {
            dbConfig2 dbcon1 = new dbConfig2();
            dbcon1.ShowDialog();
        }

        private void dbFTPConfig_Click(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAddFtpTime_Click(object sender, EventArgs e)
        {
            if (lstFtpTime.Items.Contains(txtFtpTime.Text))
            {
                MessageBox.Show("Same time already defined!");
            }
            else
            {
                lstFtpTime.Items.Add(txtFtpTime.Text);
            }
        }

        private void btbAddDataTransferTime_Click(object sender, EventArgs e)
        {
            if (lstDataTransferTime.Items.Contains(txtDataTransferTime.Text))
            {
                MessageBox.Show("Same time already defined!");
            }
            else
            {
                lstDataTransferTime.Items.Add(txtDataTransferTime.Text);
            }
        }

        private void btnApplyTime_Click(object sender, EventArgs e)
        {
            string sPathWithFileName = sPath + "\\FtpTimeList.TimWatch";

            System.IO.StreamWriter sw = new System.IO.StreamWriter(sPathWithFileName);

            foreach (object item in lstFtpTime.Items)
            {
                sw.WriteLine(item.ToString());
            }
            sw.Close();

            MessageBox.Show("Time list saved!");
        }

        private void btnDataTrnsferApply_Click(object sender, EventArgs e)
        {
            string sPathWithFileName = sPath + "\\DataTransferTimeList.TimWatch";

            System.IO.StreamWriter sw = new System.IO.StreamWriter(sPathWithFileName);

            foreach (object item in lstDataTransferTime.Items)
            {
                sw.WriteLine(item.ToString());
            }
            sw.Close();

            MessageBox.Show("Time list saved!");
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {           
            if (File.Exists(sPath + "\\FtpTimeList.TimWatch"))
            {
                using (StreamReader sr = new StreamReader(sPath + "\\FtpTimeList.TimWatch"))
                {
                    string line;
                    // Read and display lines from the file until the end of  
                    // the file is reached. 
                    while ((line = sr.ReadLine()) != null)
                    {
                        lstFtpTime.Items.Add(line);
                    }
                    sr.Close();
                }
            }

            if (File.Exists(sPath + "\\DataTransferTimeList.TimWatch"))
            {
                using (StreamReader sr = new StreamReader(sPath + "\\DataTransferTimeList.TimWatch"))
                {
                    string line;
                    // Read and display lines from the file until the end of  
                    // the file is reached. 
                    while ((line = sr.ReadLine()) != null)
                    {
                        lstDataTransferTime.Items.Add(line);
                    }
                    sr.Close();
                }
            }
        }

        private void lstFtpTime_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Do You Want Delete ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (lstFtpTime.SelectedItem != null)
                {
                    lstFtpTime.Items.Remove(lstFtpTime.SelectedItem);
                }
                return;
            }
        }

        private void lstDataTransferTime_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Do You Want Delete ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {
                if (lstDataTransferTime.SelectedItem != null)
                {
                    lstDataTransferTime.Items.Remove(lstDataTransferTime.SelectedItem);
                }
                return;
            }
        }

        private void ViewLog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string filepath = System.AppContext.BaseDirectory + "Logs\\ServiceLog_" + DateTime.Now.Day.ToString("d2") + DateTime.Now.Month.ToString("d2") + DateTime.Now.Year.ToString("d4") + ".txt";

            if (File.Exists(filepath))
            {
                Process.Start(filepath);
            }
        }

        private void BtnApplyInterval_Click(object sender, EventArgs e)
        {
            string sPathWithFileName = sPath + "\\DataTransferInterval.TimWatch";

            System.IO.StreamWriter sw = new System.IO.StreamWriter(sPathWithFileName);

            sw.WriteLine(txtDTInterval.Text.ToString().Trim());

            sw.Close();

            MessageBox.Show("Time Interval saved!");
        }
    }
}
