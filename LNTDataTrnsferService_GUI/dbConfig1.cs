﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LNTDataTrnsferService_GUI
{
    public partial class dbConfig1 : Form
    {
        string[] arrConString1;
        string sPath = Application.StartupPath;
        public dbConfig1()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dbConfig1_Load(object sender, EventArgs e)
        {
            if(File.Exists(sPath + "\\DBConnection1.TimWatch"))
            {
                using (StreamReader sr = new StreamReader(sPath + "\\DBConnection1.TimWatch"))
                {
                    //string line;
                    string filePath = System.AppContext.BaseDirectory + "DBConnection1.TimWatch";
                    string[] arrConString1 = File.ReadAllLines(filePath);
                    ddlDbType.SelectedIndex = 0;
                    ddlDbType.Enabled = false;
                    txtServer.Text = arrConString1[1].ToString();
                    txtUserName.Text = arrConString1[2].ToString();
                    txtPassword.Text = arrConString1[3].ToString();
                    txtDbName.Text = arrConString1[4].ToString();
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string sPathWithFileName = sPath + "\\DBConnection1.TimWatch";

            System.IO.StreamWriter sw = new System.IO.StreamWriter(sPathWithFileName);

            if (ddlDbType.SelectedItem.ToString().Trim().ToUpper() == "MS SQL")
                sw.WriteLine("MSSQL");
            else if (ddlDbType.SelectedItem.ToString().Trim().ToUpper() == "My SQL")
                sw.WriteLine("MYSQL");
            else if (ddlDbType.SelectedItem.ToString().Trim().ToUpper() == "My SQL")
                sw.WriteLine("MSAccess");
            else
            {
                MessageBox.Show("Please Select Database Type!");
                return;
            }

            sw.WriteLine(txtServer.Text.ToString());
            sw.WriteLine(txtUserName.Text.ToString());
            sw.WriteLine(txtPassword.Text.ToString());
            sw.WriteLine(txtDbName.Text.ToString());

            sw.Close();

            MessageBox.Show("Time list saved!");
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection1 = new SqlConnection(GetConString1()))
            {
                try
                {
                    connection1.Open();
                    MessageBox.Show("Connection successfull!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Connection faild!");
                }
            }
        }

        public string GetConString1()
        {
            string[] arrConString1 = GetConString1Arr();
            string conStringSQL1 = "Data Source =" + arrConString1[1].ToString() + ";Initial Catalog=" + arrConString1[4].ToString() + "; User ID=" + arrConString1[2].ToString() + "; Password=" + arrConString1[3].ToString() + "";

            return conStringSQL1;
        }
        public string[] GetConString1Arr()
        {
            string filePath = System.AppContext.BaseDirectory + "DBConnection1.TimWatch";

            try
            {
                arrConString1 = File.ReadAllLines(filePath);
            }
            catch (Exception ex)
            {

            }
            return arrConString1;
        }
    }
}
