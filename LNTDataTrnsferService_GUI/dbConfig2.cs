﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LNTDataTrnsferService_GUI
{
    public partial class dbConfig2 : Form
    {
        
        string[] arrConString2;

        string sPath = Application.StartupPath;
        public dbConfig2()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string sPathWithFileName = sPath + "\\DBConnection2.TimWatch";

            System.IO.StreamWriter sw = new System.IO.StreamWriter(sPathWithFileName);

            if (ddlDbType.SelectedItem.ToString().Trim().ToUpper() == "MS SQL")
                sw.WriteLine("MSSQL");
            else if (ddlDbType.SelectedItem.ToString().Trim().ToUpper() == "My SQL")
                sw.WriteLine("MYSQL");
            else
            {
                MessageBox.Show("Please Select Database Type!");
                return;
            }

            sw.WriteLine(txtServer.Text.ToString());
            sw.WriteLine(txtUserName.Text.ToString());
            sw.WriteLine(txtPassword.Text.ToString());
            sw.WriteLine(txtDbName.Text.ToString());

            sw.Close();

            MessageBox.Show("Time list saved!");
        }

        private void dbConfig2_Load(object sender, EventArgs e)
        {
            if (File.Exists(sPath + "\\DBConnection2.TimWatch"))
            {
                using (StreamReader sr = new StreamReader(sPath + "\\DBConnection2.TimWatch"))
                {
                    //string line;
                    string filePath = System.AppContext.BaseDirectory + "DBConnection2.TimWatch";
                    string[] arrConString2 = File.ReadAllLines(filePath);
                    ddlDbType.SelectedIndex = 0;
                    ddlDbType.Enabled = false;

                    txtServer.Text = arrConString2[1].ToString();
                    txtUserName.Text = arrConString2[2].ToString();
                    txtPassword.Text = arrConString2[3].ToString();
                    txtDbName.Text = arrConString2[4].ToString();
                }
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection1 = new SqlConnection(GetConString2()))
            {
                try
                {
                    connection1.Open();
                    MessageBox.Show("Connection successfull!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Connection faild!");
                }
            }
        }
        
        public string GetConString2()
        {
            string[] arrConString2 = GetConString2Arr();
            string conStringSQL2 = "Data Source=" + arrConString2[1].ToString() + ";Initial Catalog=" + arrConString2[4].ToString() + "; User ID=" + arrConString2[2].ToString() + "; Password=" + arrConString2[3].ToString() + "";

            return conStringSQL2;
        }
       
        public string[] GetConString2Arr()
        {
            //string filePath = Directory.GetCurrentDirectory() + "\\DBConnection2.TimWatch";
            string filePath = System.AppContext.BaseDirectory + "DBConnection2.TimWatch";
            try
            {
                arrConString2 = File.ReadAllLines(filePath);
            }
            catch (Exception ex)
            {
               
            }
            return arrConString2;
        }
    }
}
