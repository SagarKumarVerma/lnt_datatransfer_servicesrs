﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LNTDataTrnsferService_GUI
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(txtUserName.Text.ToString().ToUpper().Trim()=="ADMINISTRATOR" && txtPassword.Text.ToString().Trim() == "Admin@123")
            {
                this.Close();
                Dashboard dbcon1 = new Dashboard();
                dbcon1.ShowDialog();                
            }
            else
            {
                MessageBox.Show("Invalid user name or password!");
                Application.Exit();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
