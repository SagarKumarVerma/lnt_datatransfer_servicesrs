﻿namespace LNTDataTrnsferService_GUI
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.Home = new DevExpress.XtraBars.Navigation.NavigationPane();
            this.navigationPage1 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnApplyTime = new System.Windows.Forms.Button();
            this.lstFtpTime = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddFtpTime = new System.Windows.Forms.Button();
            this.txtFtpTime = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnDataTrnsferApply = new System.Windows.Forms.Button();
            this.lstDataTransferTime = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btbAddDataTransferTime = new System.Windows.Forms.Button();
            this.txtDataTransferTime = new System.Windows.Forms.TextBox();
            this.navigationPage2 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.navigationPage3 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.navigationPage4 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.navigationPage5 = new DevExpress.XtraBars.Navigation.NavigationPage();
            ((System.ComponentModel.ISupportInitialize)(this.Home)).BeginInit();
            this.Home.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // Home
            // 
            this.Home.Appearance.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Home.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Home.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Home.Appearance.Options.UseBackColor = true;
            this.Home.Appearance.Options.UseBorderColor = true;
            this.Home.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Home.AppearanceButton.Hovered.BackColor2 = System.Drawing.Color.White;
            this.Home.AppearanceButton.Hovered.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Home.AppearanceButton.Hovered.Options.UseBackColor = true;
            this.Home.AppearanceButton.Hovered.Options.UseFont = true;
            this.Home.AppearanceButton.Hovered.Options.UseTextOptions = true;
            this.Home.AppearanceButton.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Home.AppearanceButton.Normal.BackColor = System.Drawing.Color.Salmon;
            this.Home.AppearanceButton.Normal.BackColor2 = System.Drawing.Color.RosyBrown;
            this.Home.AppearanceButton.Normal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Home.AppearanceButton.Normal.Options.UseBackColor = true;
            this.Home.AppearanceButton.Normal.Options.UseBorderColor = true;
            this.Home.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Home.BackgroundImage")));
            this.Home.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Home.Controls.Add(this.navigationPage1);
            this.Home.Controls.Add(this.navigationPage2);
            this.Home.Controls.Add(this.navigationPage3);
            this.Home.Controls.Add(this.navigationPage4);
            this.Home.Controls.Add(this.navigationPage5);
            this.Home.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Home.Location = new System.Drawing.Point(0, 0);
            this.Home.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Home.Name = "Home";
            this.Home.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.navigationPage1,
            this.navigationPage2,
            this.navigationPage3,
            this.navigationPage4,
            this.navigationPage5});
            this.Home.RegularSize = new System.Drawing.Size(711, 468);
            this.Home.SelectedPage = this.navigationPage1;
            this.Home.Size = new System.Drawing.Size(711, 468);
            this.Home.TabIndex = 0;
            this.Home.Text = "Home";
            // 
            // navigationPage1
            // 
            this.navigationPage1.Caption = "Home";
            this.navigationPage1.Controls.Add(this.panel1);
            this.navigationPage1.Enabled = true;
            this.navigationPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.navigationPage1.Name = "navigationPage1";
            this.navigationPage1.Size = new System.Drawing.Size(493, 398);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(493, 398);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(493, 398);
            this.panel2.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnApplyTime);
            this.panel3.Controls.Add(this.lstFtpTime);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.btnAddFtpTime);
            this.panel3.Controls.Add(this.txtFtpTime);
            this.panel3.Location = new System.Drawing.Point(79, 87);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(164, 226);
            this.panel3.TabIndex = 8;
            // 
            // btnApplyTime
            // 
            this.btnApplyTime.Location = new System.Drawing.Point(76, 128);
            this.btnApplyTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnApplyTime.Name = "btnApplyTime";
            this.btnApplyTime.Size = new System.Drawing.Size(63, 28);
            this.btnApplyTime.TabIndex = 5;
            this.btnApplyTime.Text = "Apply";
            this.btnApplyTime.UseVisualStyleBackColor = true;
            // 
            // lstFtpTime
            // 
            this.lstFtpTime.FormattingEnabled = true;
            this.lstFtpTime.ItemHeight = 16;
            this.lstFtpTime.Location = new System.Drawing.Point(28, 82);
            this.lstFtpTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lstFtpTime.Name = "lstFtpTime";
            this.lstFtpTime.Size = new System.Drawing.Size(47, 116);
            this.lstFtpTime.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "FTP Transfer Time";
            // 
            // btnAddFtpTime
            // 
            this.btnAddFtpTime.Location = new System.Drawing.Point(76, 49);
            this.btnAddFtpTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddFtpTime.Name = "btnAddFtpTime";
            this.btnAddFtpTime.Size = new System.Drawing.Size(63, 28);
            this.btnAddFtpTime.TabIndex = 3;
            this.btnAddFtpTime.Text = "Add";
            this.btnAddFtpTime.UseVisualStyleBackColor = true;
            // 
            // txtFtpTime
            // 
            this.txtFtpTime.Location = new System.Drawing.Point(28, 50);
            this.txtFtpTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFtpTime.Name = "txtFtpTime";
            this.txtFtpTime.Size = new System.Drawing.Size(47, 22);
            this.txtFtpTime.TabIndex = 2;
            this.txtFtpTime.Text = "00:00";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnDataTrnsferApply);
            this.panel4.Controls.Add(this.lstDataTransferTime);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.btbAddDataTransferTime);
            this.panel4.Controls.Add(this.txtDataTransferTime);
            this.panel4.Location = new System.Drawing.Point(245, 87);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(164, 226);
            this.panel4.TabIndex = 7;
            // 
            // btnDataTrnsferApply
            // 
            this.btnDataTrnsferApply.Location = new System.Drawing.Point(76, 129);
            this.btnDataTrnsferApply.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDataTrnsferApply.Name = "btnDataTrnsferApply";
            this.btnDataTrnsferApply.Size = new System.Drawing.Size(63, 28);
            this.btnDataTrnsferApply.TabIndex = 5;
            this.btnDataTrnsferApply.Text = "Apply";
            this.btnDataTrnsferApply.UseVisualStyleBackColor = true;
            // 
            // lstDataTransferTime
            // 
            this.lstDataTransferTime.FormattingEnabled = true;
            this.lstDataTransferTime.ItemHeight = 16;
            this.lstDataTransferTime.Location = new System.Drawing.Point(28, 84);
            this.lstDataTransferTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lstDataTransferTime.Name = "lstDataTransferTime";
            this.lstDataTransferTime.Size = new System.Drawing.Size(47, 116);
            this.lstDataTransferTime.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data Transfer Time";
            // 
            // btbAddDataTransferTime
            // 
            this.btbAddDataTransferTime.Location = new System.Drawing.Point(76, 50);
            this.btbAddDataTransferTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btbAddDataTransferTime.Name = "btbAddDataTransferTime";
            this.btbAddDataTransferTime.Size = new System.Drawing.Size(63, 28);
            this.btbAddDataTransferTime.TabIndex = 3;
            this.btbAddDataTransferTime.Text = "Add";
            this.btbAddDataTransferTime.UseVisualStyleBackColor = true;
            // 
            // txtDataTransferTime
            // 
            this.txtDataTransferTime.Location = new System.Drawing.Point(28, 52);
            this.txtDataTransferTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDataTransferTime.Name = "txtDataTransferTime";
            this.txtDataTransferTime.Size = new System.Drawing.Size(47, 22);
            this.txtDataTransferTime.TabIndex = 2;
            this.txtDataTransferTime.Text = "00:00";
            // 
            // navigationPage2
            // 
            this.navigationPage2.Caption = "Database Configuration 1";
            this.navigationPage2.Controls.Add(this.panel5);
            this.navigationPage2.Enabled = true;
            this.navigationPage2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.navigationPage2.Name = "navigationPage2";
            this.navigationPage2.Size = new System.Drawing.Size(475, 394);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnTest);
            this.panel5.Controls.Add(this.btnSave);
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Controls.Add(this.textBox4);
            this.panel5.Controls.Add(this.textBox3);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.textBox2);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.textBox1);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.comboBox1);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(475, 394);
            this.panel5.TabIndex = 1;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(204, 274);
            this.btnTest.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(80, 28);
            this.btnTest.TabIndex = 4;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(293, 274);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(80, 28);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(113, 274);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(83, 28);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(212, 223);
            this.textBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(160, 22);
            this.textBox4.TabIndex = 2;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(212, 191);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(160, 22);
            this.textBox3.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(89, 223);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Database Name";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(212, 159);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(160, 22);
            this.textBox2.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(89, 191);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Passwors";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(212, 127);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(160, 22);
            this.textBox1.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(89, 159);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "User Name";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "MS SQL",
            "My SQL"});
            this.comboBox1.Location = new System.Drawing.Point(212, 82);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(160, 24);
            this.comboBox1.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(89, 127);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Server";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(89, 86);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Database Type";
            // 
            // navigationPage3
            // 
            this.navigationPage3.Caption = "Database Configuration 2";
            this.navigationPage3.Controls.Add(this.panel6);
            this.navigationPage3.Enabled = true;
            this.navigationPage3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.navigationPage3.Name = "navigationPage3";
            this.navigationPage3.Size = new System.Drawing.Size(475, 394);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.button1);
            this.panel6.Controls.Add(this.button2);
            this.panel6.Controls.Add(this.button3);
            this.panel6.Controls.Add(this.textBox5);
            this.panel6.Controls.Add(this.textBox6);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.textBox7);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.textBox8);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.comboBox2);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(475, 394);
            this.panel6.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(209, 273);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 28);
            this.button1.TabIndex = 4;
            this.button1.Text = "Test";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(299, 273);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 28);
            this.button2.TabIndex = 4;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(119, 273);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 28);
            this.button3.TabIndex = 3;
            this.button3.Text = "Close";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(217, 222);
            this.textBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(160, 22);
            this.textBox5.TabIndex = 2;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(217, 190);
            this.textBox6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(160, 22);
            this.textBox6.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(95, 222);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Database Name";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(217, 158);
            this.textBox7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(160, 22);
            this.textBox7.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(95, 190);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "Passwors";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(217, 126);
            this.textBox8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(160, 22);
            this.textBox8.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(95, 158);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "User Name";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "MS SQL",
            "My SQL"});
            this.comboBox2.Location = new System.Drawing.Point(217, 81);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(160, 24);
            this.comboBox2.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(95, 126);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 17);
            this.label11.TabIndex = 0;
            this.label11.Text = "Server";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(95, 85);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "Database Type";
            // 
            // navigationPage4
            // 
            this.navigationPage4.Caption = "navigationPage4";
            this.navigationPage4.Enabled = true;
            this.navigationPage4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.navigationPage4.Name = "navigationPage4";
            this.navigationPage4.Size = new System.Drawing.Size(475, 394);
            // 
            // navigationPage5
            // 
            this.navigationPage5.Caption = "navigationPage5";
            this.navigationPage5.Enabled = true;
            this.navigationPage5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.navigationPage5.Name = "navigationPage5";
            this.navigationPage5.Size = new System.Drawing.Size(475, 394);
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 468);
            this.Controls.Add(this.Home);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainPage";
            this.Text = "MainPage";
            ((System.ComponentModel.ISupportInitialize)(this.Home)).EndInit();
            this.Home.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.NavigationPane Home;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage1;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage2;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage3;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage4;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnApplyTime;
        private System.Windows.Forms.ListBox lstFtpTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAddFtpTime;
        private System.Windows.Forms.TextBox txtFtpTime;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnDataTrnsferApply;
        private System.Windows.Forms.ListBox lstDataTransferTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btbAddDataTransferTime;
        private System.Windows.Forms.TextBox txtDataTransferTime;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}