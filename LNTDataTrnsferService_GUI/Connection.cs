﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LNTDataTrnsferService_GUI
{
    class Connection
    {
        public static SqlConnection GetConnection()
        {
            //try
            //{
            string str = "";

            string sPath = Application.StartupPath;
            string sPathWithFileName = sPath + "\\DataTransferTimeList.TimWatch";

            string sDatabaseTypr = "";
            string sServerName = "";
            string sUserName = "";
            string sPassword = "";
            string sDatabaseName = "";

            if (File.Exists(sPath + "\\FtpTimeList.TimWatch"))
            {
                using (StreamReader sr = new StreamReader(sPath + "\\FtpTimeList.TimWatch"))
                {
                    string line;
                    int LineNo = 0;
                    //string[] lines;
                    //var lines = File.ReadAllLines(sPath + "\\FtpTimeList.TimWatch");

                    string[] lines = File.ReadAllLines(sPath + "\\FtpTimeList.TimWatch");

                    sDatabaseTypr = lines[0];
                    sServerName = lines[1];
                    sUserName = lines[2];
                    sPassword = lines[3];
                    sDatabaseName = lines[4];
                }
            }

            str = "Data Source=" + sServerName + ";Initial Catalog = " + sDatabaseName + ";uid =" + sUserName + ";pwd = " + sPassword + "";
            SqlConnection con = new SqlConnection(str);
            con.Open();
            return con;
            //}
            //catch
            //{
            //    MessageBox.Show("Connection Failt!");
            //    return ;
            //}
        }

    }
}
