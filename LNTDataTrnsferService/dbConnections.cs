﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace LNTDataTrnsferService
{
    class dbConnections
    {
        string ConnectionString = "";
        Cls_Log log = new Cls_Log();

        SqlConnection con1;
        SqlConnection con2;

        string[] arrConString1;
        string[] arrConString2;

        public string[] GetConString1Arr()
        {
            //log.WriteToFile("6");
            //log.WriteToFile(Environment.CurrentDirectory + "\\DBConnection1.TimWatch");

            // string filePath = @"D:\WIP\LNTDataTrnsferService\TimeWatch_GUI\TimeWatch_GUI\bin\Debug\DBConnection1.TimWatch"; //Directory.GetCurrentDirectory() + "\\DBConnection1.TimWatch";
            string filePath = System.AppContext.BaseDirectory + "DBConnection1.TimWatch";
            //log.WriteToFile(filePath);
            try
            {
                //log.WriteToFile("8");
                arrConString1 = File.ReadAllLines(filePath);
                //log.WriteToFile("9");
            }
            catch (Exception ex)
            {
                log.WriteToFile("Error----" + ex.Message.ToString());
            }
            return arrConString1;
        }
        public string[] GetConString2Arr()
        {
            //string filePath = Directory.GetCurrentDirectory() + "\\DBConnection2.TimWatch";
            string filePath = System.AppContext.BaseDirectory + "DBConnection2.TimWatch";
            try
            {
                arrConString2 = File.ReadAllLines(filePath);
            }
            catch (Exception ex)
            {
                log.WriteToFile("Error----" + ex.Message.ToString());
            }
            return arrConString2;
        }
        public string GetConString1()
        {
            string[] arrConString1 = GetConString1Arr();
            string conStringSQL1 = "Data Source =" + arrConString1[1].ToString() + ";Initial Catalog=" + arrConString1[4].ToString() + "; User ID=" + arrConString1[2].ToString() + "; Password=" + arrConString1[3].ToString() + "";

            return conStringSQL1;
        }
        public string GetConString2()
        {
            string[] arrConString2 = GetConString2Arr();
            string conStringSQL2 = "Data Source=" + arrConString2[1].ToString() + ";Initial Catalog=" + arrConString2[4].ToString() + "; User ID=" + arrConString2[2].ToString() + "; Password=" + arrConString2[3].ToString() + "";

            return conStringSQL2;
        }
        public void OpenConection1()
        {
            try
            {
                con1 = new SqlConnection(GetConString1());
                con1.Open();
            }
            catch (Exception ex)
            {
                log.WriteToFile("Error----" + ex.Message.ToString());
            }
        }
        public void OpenConection2()
        {
            con2 = new SqlConnection(GetConString1());
            con2.Open();
        }
        public void CloseConnection1()
        {
            con1.Close();
        }
        public void CloseConnection2()
        {
            con2.Close();
        }
        public void ExecuteQueries1(string Query_)
        {
            using (SqlConnection connection1 = new SqlConnection(GetConString1()))
            {
                connection1.Open();
                SqlCommand cmd = new SqlCommand(Query_, connection1);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
        }
        public void ExecuteQueries2(string Query_)
        {
            using (SqlConnection connection2 = new SqlConnection(GetConString2()))
            {
                try
                {
                    connection2.Open();
                    SqlCommand cmd = new SqlCommand(Query_, connection2);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
                catch (Exception ex)
                {
                    log.WriteToFile("Error:- " + ex.Message.ToString());
                }
            }
        }
        public SqlDataReader DataReader(string Query_)
        {
            using (SqlConnection connection1 = new SqlConnection(GetConString1()))
            {
                connection1.Open();
                SqlCommand cmd = new SqlCommand(Query_, connection1);
                SqlDataReader dr = cmd.ExecuteReader();
                return dr;
                //connection1.Close();
            }
        }
        public DataTable SelectAllData(string Query_)
        {
            //log.WriteToFile("3");
            using (SqlConnection connection1 = new SqlConnection(GetConString1()))
            {
                //log.WriteToFile("4");
                try
                {
                    connection1.Open();
                    //log.WriteToFile("Source Database Connected!");
                }
                catch (Exception ex)
                {
                    log.WriteToFile("Source Database Connection Faild Due to error:- " + ex.Message);
                }

                //log.WriteToFile("5");

                SqlDataAdapter dr = new SqlDataAdapter(Query_, connection1);
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                dr.Fill(dt);
                //object dataum = ds.Tables[0];
                return dt;
            }
        }

        public DataTable SelectDataFromDestDB(string Query_)
        {
            //log.WriteToFile("3");
            using (SqlConnection connection2 = new SqlConnection(GetConString2()))
            {
                //log.WriteToFile("4");
                try
                {
                    connection2.Open();
                    //log.WriteToFile("Source Database Connected!");
                }
                catch (Exception ex)
                {
                    log.WriteToFile("Destination Database Connection Faild Due to error:- " + ex.Message);
                }

                //log.WriteToFile("5");

                SqlDataAdapter dr = new SqlDataAdapter(Query_, connection2);
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                dr.Fill(dt);
                //object dataum = ds.Tables[0];
                return dt;
            }
        }
    }
}
