﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LNTDataTrnsferService
{
    public class cls_init
    {
        Cls_Log lg = new Cls_Log();
        static string[] arrDataTransferTime;
        static string strInterval = "";
        static string IsQueryPrint = "N";
        //string[] arrConString2;
        public static string[] DataTransferTime(string strPath)
        {
            try
            {
                //string filePath = @"D:\WIP\LNTDataTrnsferService\TimeWatch_GUI\TimeWatch_GUI\bin\Debug\DataTransferTimeList.TimWatch";// Directory.GetCurrentDirectory() + "\\DataTransferTimeList.TimWatch";
                arrDataTransferTime = File.ReadAllLines(strPath);
            }
            catch (Exception ex)
            {
                //Cls_Log.lWriteToFile("Data Transfer Start.");
            }
            return arrDataTransferTime;
        }
        public static string DataTransferInterval(string strPath)
        {
            try
            {
                //string filePath = @"D:\WIP\LNTDataTrnsferService\TimeWatch_GUI\TimeWatch_GUI\bin\Debug\DataTransferTimeList.TimWatch";// Directory.GetCurrentDirectory() + "\\DataTransferTimeList.TimWatch";
                strInterval = File.ReadAllText(strPath);
            }
            catch (Exception ex)
            {
                //Cls_Log.lWriteToFile("Data Transfer Start.");
            }
            return strInterval;
        }
        public static string PrintQuery(string strPath)
        {
            try
            {
                IsQueryPrint = File.ReadAllText(strPath);
            }
            catch (Exception ex)
            {
                IsQueryPrint = "N";
            }
            return IsQueryPrint;
        }
    }
}
