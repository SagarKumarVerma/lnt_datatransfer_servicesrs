﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LNTDataTrnsferService
{
    class DataTransfer
    {
        //string strPath = System.AppContext.BaseDirectory + "DataTransferInterval.TimWatch";
        string IsQueryPrint = cls_init.PrintQuery(System.AppContext.BaseDirectory + "IsQueryPrint.TimWatch").ToString().Trim();

        Cls_Log log = new Cls_Log();

        dbConnections dbcon = new dbConnections();
        //dbConnections dbcon = new dbConnections();
        public void DataTrnf()
        {
            string strRowID = "";
            string strDeviceID = "";
            string strUserID = "";
            string strAttState = "";
            string strVerifyMode = "";
            string strWorkCode = "";
            string strAttDateTime = "";
            string strUpdateedOn = "";
            string strCTemp = "";
            string strFTemp = "";
            string strMaskStatus = "";
            string strIsAbnomal = "";
            string strLImage = "";
            string strTImage = "";
            string strio_workcode = "";
            string strverify_mode = "";
            string strio_mode = "";
            int intUpload = 0;
            string strTransferFlag = "";
            string strJobCode = "";
            string strCompanyName = "";

            string strTableName = "";

            string strChkTable = "0";

            string strQueryComp = "select top 1 COMPANYCODE from tblCompany";
            
            if (IsQueryPrint.ToString().ToUpper().Trim() == "Y")
                log.WriteToFile(strQueryComp);
            
            using (var dtcomp = dbcon.SelectAllData(strQueryComp))
            {
                strTableName = "UserAttendance_" + dtcomp.Rows[0]["COMPANYCODE"].ToString().Trim();
            }
            string strQueryCheckTable = "IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='" + strTableName + "') SELECT 1 AS res ELSE SELECT 0 AS res;";

            using (var dt2 = dbcon.SelectDataFromDestDB(strQueryCheckTable))
            {
                if (dt2.Rows[0]["res"].ToString().Trim() == "0")
                {
                    string strCreateQuery = "CREATE TABLE " + strTableName + "([RowID] [bigint] IDENTITY(1,1) NOT NULL,	[DeviceID] [varchar](50) NOT NULL,	[UserID] [varchar](50) NOT NULL,	[AttState] [varchar](50) NULL,	[VerifyMode] [varchar](50) NULL,	[WorkCode] [varchar](50) NULL,	[AttDateTime] [datetime] NOT NULL,	[UpdateedOn] [datetime] NULL,	[CTemp] [varchar](50) NULL,	[FTemp] [varchar](50) NULL,	[MaskStatus] [varchar](50) NULL,	[IsAbnomal] [varchar](50) NULL,	[LImage] [varbinary](max) NULL,	[TImage] [varbinary](max) NULL,	[io_workcode] [varchar](50) NULL,	[verify_mode] [varchar](50) NULL,	[io_mode] [varchar](50) NULL,	[Upload] [int] NULL,	[JobCode] [varchar](32) NULL,	[CompanyName] [varchar](32) NULL,CONSTRAINT [PK_" + strTableName + "] PRIMARY KEY NONCLUSTERED (	[DeviceID] ASC,	[UserID] ASC,	[AttDateTime] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
                    dbcon.ExecuteQueries2(strCreateQuery);
                }
            }

            string strQuery = "select RowID,DeviceID,UserID,AttState,VerifyMode,WorkCode,tblcompany.COMPANYCODE,(convert(varchar(10),convert(datetime,AttDateTime,103),103)+' '+convert(varchar(10),convert(datetime,AttDateTime,108),108))AttDateTime,(convert(varchar(10),convert(datetime,UpdateedOn,103),103)+' '+convert(varchar(10),convert(datetime,UpdateedOn,108),108))UpdateedOn,CTemp,FTemp,MaskStatus,IsAbnomal,LImage,TImage,io_workcode,verify_mode,io_mode,Upload,UserAttendance.TransferFlag,DepartmentName JobCode,tblcompany.COMPANYNAME from UserAttendance,tblcompany,tblmachine,tblDepartment where deviceID=SerialNumber and tblcompany.companycode=tblmachine.companycode AND tblDepartment.DepartmentCode=branch and UserAttendance.TransferFlag='N'";// where TransferFlag='N'";
            //dbcon.DataReader(strQuery);

            if (IsQueryPrint.ToString().ToUpper().Trim() == "Y")
                log.WriteToFile(strQuery);

            using (var dt = dbcon.SelectAllData(strQuery))
            {
                //log.WriteToFile("1");
                try
                {
                    if (dt.Rows.Count == 0)
                    {
                        log.WriteToFile("New Data Not Found!");
                    }
                    else
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //log.WriteToFile("2");

                            strRowID = dt.Rows[i]["RowID"].ToString().Trim();
                            strDeviceID = dt.Rows[i]["DeviceID"].ToString().Trim();
                            strUserID = dt.Rows[i]["UserID"].ToString().Trim();

                            if (dt.Rows[i]["AttState"].ToString().Trim() != "")
                                strAttState = dt.Rows[i]["AttState"].ToString().Trim();
                            else
                                strAttState = "0";

                            strVerifyMode = dt.Rows[i]["VerifyMode"].ToString().Trim();

                            if (dt.Rows[i]["WorkCode"].ToString().Trim() != "")
                                strWorkCode = dt.Rows[i]["WorkCode"].ToString().Trim();
                            else
                                strWorkCode = "0";

                            strAttDateTime = dt.Rows[i]["AttDateTime"].ToString().Trim();
                            strUpdateedOn = dt.Rows[i]["UpdateedOn"].ToString().Trim();
                            strCTemp = dt.Rows[i]["CTemp"].ToString().Trim();
                            strFTemp = dt.Rows[i]["FTemp"].ToString().Trim();
                            strMaskStatus = dt.Rows[i]["MaskStatus"].ToString().Trim();
                            strIsAbnomal = dt.Rows[i]["IsAbnomal"].ToString().Trim();

                            //if (strLImage != "")
                            //    strLImage = "convert(varbinary, "+ dt.Rows[i]["LImage"].ToString().Trim() + ")"  ;
                            //else
                            //    strLImage = null;

                            //if (strTImage != "")
                            //    strTImage = "convert(varbinary, "+ dt.Rows[i]["TImage"].ToString().Trim() + ")"  ;
                            //else
                            //    strTImage = null;

                            //strTImage = dt.Rows[i]["TImage"].ToString().Trim();

                            strio_workcode = dt.Rows[i]["io_workcode"].ToString().Trim();
                            strverify_mode = dt.Rows[i]["verify_mode"].ToString().Trim();
                            strio_mode = dt.Rows[i]["io_mode"].ToString().Trim();

                            //log.WriteToFile("21");

                            if (dt.Rows[i]["Upload"].ToString().Trim() != "")
                                intUpload = Convert.ToInt32(dt.Rows[i]["Upload"].ToString().Trim());
                            else
                                intUpload = 0;

                            //log.WriteToFile("22");

                            strTransferFlag = dt.Rows[i]["TransferFlag"].ToString().Trim();
                            strJobCode = dt.Rows[i]["JobCode"].ToString().Trim();
                            strCompanyName = dt.Rows[i]["companyname"].ToString().Trim();

                            //log.WriteToFile("23");

                            //string strQueryIns = "insert into UserAttendance (DeviceID,UserID,AttState,VerifyMode,WorkCode,AttDateTime,UpdateedOn,CTemp,FTemp,MaskStatus,IsAbnomal,LImage,TImage,io_workcode,verify_mode,io_mode,Upload,JobCode,CompanyName) " +
                            //    "values('" + strDeviceID + "','" + strUserID + "','" + strAttState + "','" + strVerifyMode + "'," + strWorkCode + ",convert(datetime,'" + strAttDateTime + "',103),convert(datetime,'" + strUpdateedOn + "',103),'" + strCTemp + "','" + strFTemp + "','" + strMaskStatus + "','" + strIsAbnomal + "',convert(varbinary,'" + strLImage + "'),convert(varbinary,'" + strTImage + "'),'" + strio_workcode + "','" + strverify_mode + "','" + strio_mode + "'," + intUpload + ",'" + strJobCode + "','" + strCompanyName + "')";

                            string strQueryIns = "insert into " + strTableName + " (DeviceID,UserID,AttState,VerifyMode,WorkCode,AttDateTime,UpdateedOn,CTemp,FTemp,MaskStatus,IsAbnomal,io_workcode,verify_mode,io_mode,Upload,JobCode,CompanyName) " +
                                "values('" + strDeviceID + "','" + strUserID + "','" + strAttState + "','" + strVerifyMode + "'," + strWorkCode + ",convert(datetime,'" + strAttDateTime + "',103),convert(datetime,'" + strUpdateedOn + "',103),'" + strCTemp + "','" + strFTemp + "','" + strMaskStatus + "','" + strIsAbnomal + "','" + strio_workcode + "','" + strverify_mode + "','" + strio_mode + "'," + intUpload + ",'" + strJobCode + "','" + strCompanyName + "')";

                            if (IsQueryPrint.ToString().ToUpper().Trim() == "Y")
                                log.WriteToFile(strQueryIns);

                            dbcon.ExecuteQueries2(strQueryIns);

                            //log.WriteToFile("24");

                            string strQueryUpd = "Update UserAttendance set TransferFlag='Y' where DeviceID='" + strDeviceID + "' and UserID='" + strUserID + "' and AttDateTime=convert(datetime,'" + strAttDateTime + "',103)";

                            if (IsQueryPrint.ToString().ToUpper().Trim() == "Y")
                                log.WriteToFile(strQueryUpd);

                            dbcon.ExecuteQueries1(strQueryUpd);
                        }
                        log.WriteToFile("Data Transfered to table " + strTableName);
                    }
                }
                catch (Exception ex)
                {
                    log.WriteToFile("Error----" + ex.Message.ToString());
                }
            }
        }

        public void MachineMasterDataTrnf()
        {
            string sIN_OUT = "";
            string sLOCATION = "";
            string sDeviceName = "";
            string sSerialNumber = "";
            string sIdNo = "";
            string sCompanyCode = "";

            string strQueryMachine = "select * from tblMachine where TransferFlag='N'";
            
            if (IsQueryPrint.ToString().ToUpper().Trim() == "Y")
                log.WriteToFile(strQueryMachine);

            using (var dt = dbcon.SelectAllData(strQueryMachine))
            {
                //log.WriteToFile("1");
                try
                {
                    if (dt.Rows.Count == 0)
                    {
                        log.WriteToFile("New Device Not Found!");
                    }
                    else
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //log.WriteToFile("2");

                            sIN_OUT = dt.Rows[i]["IN_OUT"].ToString().Trim();
                            sLOCATION = dt.Rows[i]["LOCATION"].ToString().Trim();
                            sDeviceName = dt.Rows[i]["DeviceName"].ToString().Trim();
                            sSerialNumber = dt.Rows[i]["SerialNumber"].ToString().Trim();
                            sIdNo = dt.Rows[i]["ID_NO"].ToString().Trim();
                            sCompanyCode = dt.Rows[i]["CompanyCode"].ToString().Trim();

                            string strQueryIns = "insert into tblmachine (ID_NO,IN_OUT,LOCATION,DeviceName,SerialNumber,CompanyCode) " +
                                "values('" + sIdNo + "','" + sIN_OUT + "','" + sLOCATION + "','" + sDeviceName + "','" + sSerialNumber + "','" + sCompanyCode + "')";

                            if (IsQueryPrint.ToString().ToUpper().Trim() == "Y")
                                log.WriteToFile(strQueryIns);

                            dbcon.ExecuteQueries2(strQueryIns);

                            //log.WriteToFile("24");

                            string strQueryUpd = "Update tblmachine set TransferFlag='Y' where ID_NO='" + sIdNo + "'";

                            if (IsQueryPrint.ToString().ToUpper().Trim() == "Y")
                                log.WriteToFile(strQueryUpd);

                            dbcon.ExecuteQueries1(strQueryUpd);

                            log.WriteToFile("Machine with ID " + sIdNo + " added in tblMachine");
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.WriteToFile("Error----" + ex.Message.ToString());
                }
            }
        }
    }
}
