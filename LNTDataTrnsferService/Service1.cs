﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LNTDataTrnsferService
{
    public partial class Service1 : ServiceBase
    {

        //string[] arrTransferTime = cls_init.DataTransferTime();
        string currTime = DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString();

        Timer timer = new Timer(); // name space(using System.Timers;)
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteToFile("Service is started at " + DateTime.Now);
            string strPath = System.AppContext.BaseDirectory + "DataTransferInterval.TimWatch";
            string strInterval = cls_init.DataTransferInterval(strPath).ToString().Trim();

            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = (Convert.ToInt32(strInterval)) * 60000; //number in milisecinds
            timer.Enabled = true;
            WriteToFile("Interval Time - " + timer.Interval.ToString());
        }

        protected override void OnStop()
        {
            WriteToFile("Service is stopped at " + DateTime.Now);
        }

        //public void test1()
        //{
        //    string[] arrTransferTime = cls_init.DataTransferTime();
        //    WriteToFile("Current Time......." + DateTime.Now.Hour.ToString("d2") + ":" + DateTime.Now.Minute.ToString("d2"));
        //    WriteToFile("Service is recall at " + DateTime.Now);

        //    WriteToFile(arrTransferTime[0].ToString());

        //    if (arrTransferTime.Contains(DateTime.Now.Hour.ToString("d2") + ":" + DateTime.Now.Minute.ToString("d2")))
        //    {
        //        WriteToFile("Data Transfer Start.");
        //        DataTransfer dt = new DataTransfer();
        //        dt.DataTrnf();
        //    }
        //}
        public void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                //string strPath = @"D:\WIP\LNTDataTrnsferService\TimeWatch_GUI\TimeWatch_GUI\bin\Debug\DataTransferTimeList.TimWatch";
                string strPath = System.AppContext.BaseDirectory + "DataTransferTimeList.TimWatch";

                //WriteToFile(strPath);
                string[] arrTransferTime = cls_init.DataTransferTime(strPath);
                //WriteToFile("hi");


                DataTransfer dt = new DataTransfer();

                WriteToFile("Device Transfer start at " + DateTime.Now.Hour.ToString("d2") + ":" + DateTime.Now.Minute.ToString("d2"));
                dt.MachineMasterDataTrnf();

                WriteToFile("Data Transfer Start at " + DateTime.Now.Hour.ToString("d2") + ":" + DateTime.Now.Minute.ToString("d2"));
                dt.DataTrnf();

                //if (arrTransferTime.Contains(DateTime.Now.Hour.ToString("d2") + ":" + DateTime.Now.Minute.ToString("d2")))
                //{
                //    WriteToFile("Data Transfer Start.");
                //    DataTransfer dt = new DataTransfer();
                //    dt.DataTrnf();
                //}
            }
            catch (Exception ex)
            {
                WriteToFile("Error----" + ex.ToString());
            }

        }

        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Day.ToString("d2") + DateTime.Now.Month.ToString("d2") + DateTime.Now.Year.ToString("d4") + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to. 
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}
